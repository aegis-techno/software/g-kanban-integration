import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BlankLayoutComponent} from '@app/layouts/components/blank-layout/blank-layout.component';
import {ErrorComponent} from '@app/layouts/components/error/error.component';
import {LayoutComponent} from '@app/layouts/components/layout/layout.component';

export const AppRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
    },
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./modules/kanban/kanban.module').then((m) => m.KanbanModule),
                data:{
                    title: 'Dashboard'
                }
            },
            {
                path: 'stats',
                loadChildren: () => import('./modules/statistics/statistics.module').then(m => m.StatisticsModule),
                data:{
                    title: 'Statistiques'
                }
            },
            {
                path: 'config',
                loadChildren: () => import('./modules/configuration/configuration.module').then(m => m.ConfigurationModule),
                data:{
                    title: 'Configuration'
                }
            },
        ]
    },
    {
        path: 'error',
        component: BlankLayoutComponent,
        children: [
            {path: '404', component: ErrorComponent, pathMatch: 'full'},
            {path: '**', redirectTo: '404'}
        ]
    },
    {path: '**', redirectTo: '/error/404'}

];

@NgModule({
    imports: [
        RouterModule.forRoot(AppRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}
