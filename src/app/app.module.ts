import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutsModule} from '@app/layouts/layouts.module';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {EventBusService} from "@aegis-techno/ngx-common";
import {NgxLocalStorageModule} from "@aegis-techno/ngx-local-storage";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSnackBarModule} from "@angular/material/snack-bar";

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        LayoutsModule,
        NgxLocalStorageModule.forRoot({
            prefix: "GKAT"
        }),

        MatDialogModule,
        MatSnackBarModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        {
            provide: LocationStrategy,
            useClass: PathLocationStrategy
        },
        Location
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}
