import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SideBarService} from '@app/layouts/components/sidebar/side-bar.service';
import {Optional} from "@aegis-techno/ngx-common";

@Component({
               selector: 'app-admin-layout',
               templateUrl: './layout.component.html',
               styleUrls: ['./layout.component.scss']
           })
export class LayoutComponent implements OnInit {

    public sideNavIsClose: Optional<boolean>;

    constructor(private router: Router, private sideBarService: SideBarService) {
    }

    ngOnInit() {
        this.sideBarService.isCloseSubject.subscribe(value => {
            this.sideNavIsClose = value;
        });
    }

}
