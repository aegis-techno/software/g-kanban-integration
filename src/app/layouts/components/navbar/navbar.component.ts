import {Component} from '@angular/core';
import {Observable, of} from "rxjs";
import {BaseComponent} from "@aegis-techno/ngx-common";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {filter, map, mapTo, pluck, startWith, switchMap, tap} from "rxjs/operators";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent extends BaseComponent {

    $title: Observable<string> = of('');

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        super();
        this.$title = this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            startWith(null),
            mapTo(this.router.routerState.root),
            map(route => {
                while(route.firstChild){
                    route = route.firstChild
                }
                return route
            }),
            switchMap((route) => route.data),
            pluck('title'),

        )

    }

}
