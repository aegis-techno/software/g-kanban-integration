import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class SideBarService {
    public isCloseSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


}
