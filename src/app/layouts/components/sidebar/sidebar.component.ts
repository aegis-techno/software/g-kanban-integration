import {Component, OnInit} from '@angular/core';
import {filter, pluck} from "rxjs/operators";
import {Objects} from "@aegis-techno/ngx-common";
import {Observable} from "rxjs";
import {KanbanBoard} from "@app/shared/models/kanban-board";
import {DataStorageService} from "@app/shared/services/data-storage.service";

declare const $: any;

@Component({
               selector: 'app-sidebar',
               templateUrl: './sidebar.component.html',
               styleUrls: ['./sidebar.component.css']
           })
export class SidebarComponent implements OnInit {
    public $boards: Observable<KanbanBoard[]> | undefined;

    constructor(private dataStorageService: DataStorageService) {

    }

    ngOnInit() {
        this.$boards = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            pluck('boards'),
        );
    }
}
