import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {RouterModule} from '@angular/router';
import {BlankLayoutComponent} from '@app/layouts/components/blank-layout/blank-layout.component';
import {FooterComponent} from '@app/layouts/components/footer/footer.component';
import {LayoutComponent} from '@app/layouts/components/layout/layout.component';
import {NavbarComponent} from '@app/layouts/components/navbar/navbar.component';
import {ErrorComponent} from './components/error/error.component';

import {SidebarComponent} from './components/sidebar/sidebar.component';

@NgModule({
              imports: [
                  CommonModule,
                  RouterModule,
                  MatButtonModule
              ],
              declarations: [
                  FooterComponent,
                  NavbarComponent,
                  SidebarComponent,
                  BlankLayoutComponent,
                  LayoutComponent,
                  ErrorComponent
              ],
              exports: [
                  FooterComponent,
                  NavbarComponent,
                  SidebarComponent,
                  BlankLayoutComponent,
                  LayoutComponent
              ]
          })
export class LayoutsModule {
}
