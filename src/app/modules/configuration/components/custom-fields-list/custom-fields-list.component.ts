import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {filter, pluck, tap} from "rxjs/operators";
import {BaseComponent, Objects} from "@aegis-techno/ngx-common";
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {noop, Observable} from "rxjs";
import {CustomField} from "@app/shared/models/custom-field";
import {CustomFieldService} from "@app/modules/configuration/servcies/custom-field.service";
import {MatTable} from "@angular/material/table";

@Component({
    selector: 'app-custom-fields-list',
    templateUrl: './custom-fields-list.component.html',
    styleUrls: ['./custom-fields-list.component.scss']
})
export class CustomFieldsListComponent extends BaseComponent implements OnInit {

    public $customFields: Observable<CustomField[]> | undefined;
    public displayedColumns: string[];
    public typeOption: any[] = [
        {
            label: 'Text',
            value: 'text'
        }, {
            label: 'Number',
            value: 'number'
        }, {
            label: 'Checkbox',
            value: 'boolean'
        }
    ];

    @ViewChild('matTable')
    public matTable: MatTable<any> | undefined;

    constructor(
        private dataStorageService: DataStorageService,
        private customFieldService: CustomFieldService
    ) {
        super()
        this.displayedColumns = [
            'label',
            'type',
            'defaultValue',
            'visibility',
            'actions'
        ]
    }

    ngOnInit(): void {

        this.$customFields = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            pluck('customFields'),
        );
    }

    save(customField: CustomField, param: string, value: any) {
        this.subscribe(this.customFieldService.save(customField, param, value), noop)
    }

    create() {
        this.subscribe(this.customFieldService.create(), () => {
            this.matTable?.renderRows();
        })
    }

    remove(customField: CustomField) {
        this.subscribe(this.customFieldService.remove(customField), () => {
            this.matTable?.renderRows();
        })

    }
}
