import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigurationHomeComponent } from './components/configuration-home/configuration-home.component';

const routes: Routes = [{ path: '', component: ConfigurationHomeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
