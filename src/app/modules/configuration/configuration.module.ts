import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationRoutingModule } from './configuration-routing.module';
import { ConfigurationHomeComponent } from './components/configuration-home/configuration-home.component';
import {MatTableModule} from "@angular/material/table";
import { CustomFieldsListComponent } from './components/custom-fields-list/custom-fields-list.component';
import {MatIconModule} from "@angular/material/icon";
import {MatTabsModule} from "@angular/material/tabs";
import {SharedModule} from "@app/shared/shared.module";


@NgModule({
  declarations: [
    ConfigurationHomeComponent,
    CustomFieldsListComponent
  ],
    imports: [
        CommonModule,
        ConfigurationRoutingModule,
        MatTableModule,
        MatIconModule,
        MatTabsModule,
        SharedModule
    ]
})
export class ConfigurationModule { }
