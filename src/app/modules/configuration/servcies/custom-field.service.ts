import {Injectable} from "@angular/core";
import {CustomField} from "@app/shared/models/custom-field";
import {of} from "rxjs";
import {tap} from "rxjs/operators";
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {Utils} from "@app/shared/tools/utils";

@Injectable({providedIn: "root"})
export class CustomFieldService {
    constructor(private dataStorageService: DataStorageService) {
    }

    save(customField: CustomField, param: string, value: any) {
        return of(null).pipe(
            tap(() => customField[param] = value),
            tap(() => this.dataStorageService.saveSubject.next()),
        );
    }

    create() {
        return this.dataStorageService.$data.pipe(
            tap((data) => data?.customFields.push(CustomField.fromObject({id: Utils.newId()}))),
            tap(() => this.dataStorageService.saveSubject.next()),
        );
    }

    remove(customField: CustomField) {
        return this.dataStorageService.$data.pipe(
            tap((data) => data?.customFields.splice(data?.customFields.indexOf(customField),1)),
            tap(() => this.dataStorageService.saveSubject.next()),
        );
    }
}
