import {BaseComponent, Objects, Strings} from '@aegis-techno/ngx-common';
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {KanbanStoryService} from '@app/modules/kanban/services/kanban-story.service';
import {KanbanBoard} from '@app/shared/models/kanban-board';
import {noop, Observable, of, Subject} from 'rxjs';
import {catchError, filter, map, pluck, switchMap, tap} from 'rxjs/operators';
import {DataStorageService} from "@app/shared/services/data-storage.service";

@Component({
    selector: 'app-kanband-board',
    templateUrl: './kanban-board.component.html',
    styleUrls: ['./kanban-board.component.scss']
})
export class KanbanBoardComponent extends BaseComponent implements OnInit {

    public $board: Observable<KanbanBoard> | undefined;

    constructor(
        private dataStorageService: DataStorageService,
        private storyService: KanbanStoryService,
        private activatedRoute: ActivatedRoute
    ) {
        super();
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(
            filter(Objects.isNotNullOrUndefined),
            pluck('id'),
            filter((id: string) => Strings.isNotBlank(id)),
            tap((idBoard: string) => {
                this.$board = this.dataStorageService.$data.pipe(
                    filter(Objects.isNotNullOrUndefined),
                    pluck('boards'),
                    map((boards) => boards.find(board => board.id === idBoard)),
                    filter(Objects.isNotNullOrUndefined)
                );

            })
        ).subscribe()
    }
}
