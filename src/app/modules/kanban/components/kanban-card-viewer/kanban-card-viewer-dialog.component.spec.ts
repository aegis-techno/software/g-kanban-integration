import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KanbanCardViewerDialogComponent} from './kanban-card-viewer-dialog.component';

describe('CardDialogComponent', () => {
    let component: KanbanCardViewerDialogComponent;
    let fixture: ComponentFixture<KanbanCardViewerDialogComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
                                                 declarations: [KanbanCardViewerDialogComponent]
                                             })
                     .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(KanbanCardViewerDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
