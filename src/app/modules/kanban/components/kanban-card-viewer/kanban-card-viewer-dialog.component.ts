import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {KanbanCard} from "@app/shared/models/kanban-card";
import {KanbanCardService} from "@app/modules/kanban/services/kanban-card.service";
import {noop, Observable} from "rxjs";
import {BaseComponent, Objects} from "@aegis-techno/ngx-common";
import {filter, pluck} from "rxjs/operators";
import {CustomField} from "@app/shared/models/custom-field";
import {DataStorageService} from "@app/shared/services/data-storage.service";

@Component({
    selector: 'app-card-viewer-dialog',
    templateUrl: './kanban-card-viewer-dialog.component.html',
    styleUrls: ['./kanban-card-viewer-dialog.component.scss']
})
export class KanbanCardViewerDialogComponent extends BaseComponent implements OnInit {

    public $customFields: Observable<CustomField[]> | undefined;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { card: KanbanCard },
        private cardService: KanbanCardService,
        private dataStorageService: DataStorageService
    ) {
        super()
    }

    public ngOnInit(): void {

        this.$customFields = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            pluck('customFields'),
        );
    }

    save(name: string) {
        this.subscribe(
            this.cardService.updateCard(this.data.card, name),
            noop
        )
    }

    saveField(customField: CustomField, value: any) {
        this.subscribe(
            this.cardService.updateCardField(this.data.card, customField, value),
            noop
        )
    }
}
