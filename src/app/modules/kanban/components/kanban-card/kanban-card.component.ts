import {Component, Input, OnInit} from '@angular/core';
import {KanbanCard} from '@app/shared/models/kanban-card';
import {from, noop, Observable, Subject} from "rxjs";
import {filter, map, pluck, switchMap, throttleTime, toArray} from "rxjs/operators";
import {BaseComponent, Objects} from "@aegis-techno/ngx-common";
import {KanbanCardService} from "@app/modules/kanban/services/kanban-card.service";
import {KanbanStory} from "@app/shared/models/kanban-story";
import {CustomField} from "@app/shared/models/custom-field";
import {DataStorageService} from "@app/shared/services/data-storage.service";

@Component({
    selector: 'app-kanban-card',
    templateUrl: './kanban-card.component.html',
    styleUrls: ['./kanban-card.component.scss']
})
export class KanbanCardComponent extends BaseComponent implements OnInit {

    @Input()
    public card: KanbanCard | undefined;

    @Input()
    public story: KanbanStory | undefined;

    private viewCardSubject: Subject<any> = new Subject<any>();
    private removeCardSubject: Subject<any> = new Subject<any>();
    public $customFields: Observable<CustomField[]> | undefined;

    constructor(
        private cardService: KanbanCardService,
        private dataStorageService: DataStorageService
    ) {
        super()
    }

    ngOnInit(): void {
        this.subscribe(
            this.removeCardSubject.asObservable().pipe(
                throttleTime(300),
                switchMap(() => this.cardService.removeCard(this.card!))
            ),
            noop
        );

        this.subscribe(
            this.viewCardSubject.asObservable().pipe(
                throttleTime(300),
                switchMap(() => this.cardService.viewCard(this.card!))
            ),
            noop
        );

        this.$customFields = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            pluck('customFields'),
            filter(Objects.isNotNullOrUndefined),
            switchMap((arr) => from(arr).pipe(
                filter(value => value.visibility),
                toArray()
            ))
        );
    }

    viewCard() {
        if (Objects.isNotNullOrUndefined(this.card)) {
            this.viewCardSubject.next();
        }
    }

    removeCard() {
        this.removeCardSubject.next();
    }

    save(name: string) {
        this.subscribe(
            this.cardService.saveOrCreate(this.story, this.card, name),
            noop
        );
    }
}
