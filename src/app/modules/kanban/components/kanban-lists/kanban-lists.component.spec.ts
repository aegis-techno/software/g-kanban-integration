import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KanbanListsComponent } from './kanban-lists.component';

describe('KanbanListsComponent', () => {
  let component: KanbanListsComponent;
  let fixture: ComponentFixture<KanbanListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KanbanListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KanbanListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
