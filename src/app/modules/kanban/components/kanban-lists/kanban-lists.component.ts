import {Component, OnInit} from '@angular/core';
import {noop, Observable, Subject} from "rxjs";
import {KanbanBoard} from "@app/shared/models/kanban-board";
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {filter, pluck, switchMap, throttleTime} from "rxjs/operators";
import {BaseComponent, Objects} from "@aegis-techno/ngx-common";
import {KanbanBoardService} from "@app/modules/kanban/services/kanban-board.service";

@Component({
    selector: 'app-kanban-lists',
    templateUrl: './kanban-lists.component.html',
    styleUrls: ['./kanban-lists.component.scss']
})
export class KanbanListsComponent extends BaseComponent implements OnInit {

    public $boards: Observable<KanbanBoard[]> | undefined;

    private removeSubject: Subject<any> = new Subject<any>();

    constructor(
        private dataStorageService: DataStorageService,
        private boardService: KanbanBoardService,
    ) {
        super();

        this.$boards = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            pluck('boards'),
        );
    }

    ngOnInit(): void {
        this.subscribe(
            this.removeSubject.asObservable().pipe(
                throttleTime(300),
                switchMap((board) => this.boardService.removeBoard(board))
            ),
            noop
        );
    }

    removeBoard(board: KanbanBoard) {

        this.removeSubject.next(board);
    }

    save(board: KanbanBoard, name: string) {
        this.subscribe(
            this.boardService.editBoard(board, name),
            noop
        );
    }

    create(name: string) {
        this.subscribe(
            this.boardService.createBoard(this.dataStorageService.$data.getValue()!, name),
            noop
        );
    }
}
