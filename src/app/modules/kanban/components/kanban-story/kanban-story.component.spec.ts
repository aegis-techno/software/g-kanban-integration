import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KanbanStoryComponent} from './kanban-story.component';

describe('StoryComponent', () => {
    let component: KanbanStoryComponent;
    let fixture: ComponentFixture<KanbanStoryComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
                                                 declarations: [KanbanStoryComponent]
                                             })
                     .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(KanbanStoryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
