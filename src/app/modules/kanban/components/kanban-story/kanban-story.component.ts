import {BaseComponent} from '@aegis-techno/ngx-common';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, TemplateRef} from '@angular/core';
import {KanbanCardService} from '@app/modules/kanban/services/kanban-card.service';
import {KanbanCard} from '@app/shared/models/kanban-card';
import {KanbanStory} from '@app/shared/models/kanban-story';
import {noop, Subject} from 'rxjs';
import {switchMap, throttleTime} from 'rxjs/operators';
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {KanbanStoryService} from "@app/modules/kanban/services/kanban-story.service";
import {KanbanBoard} from "@app/shared/models/kanban-board";

@Component({
    selector: 'app-kanban-story',
    templateUrl: './kanban-story.component.html',
    styleUrls: ['./kanban-story.component.scss']
})
export class KanbanStoryComponent extends BaseComponent implements OnInit {

    @Input()
    public story: KanbanStory | undefined;

    @Input()
    public board: KanbanBoard | undefined;

    private removeStorySubject: Subject<any> = new Subject<any>();

    constructor(
        private cdr: ChangeDetectorRef,
        private cardService: KanbanCardService,
        private storyService: KanbanStoryService,
        private dataStorageService: DataStorageService
    ) {
        super();
    }

    ngOnInit(): void {

        this.subscribe(
            this.removeStorySubject.asObservable().pipe(
                throttleTime(300),
                switchMap(() => this.storyService.askForRemoveStory(this.story!))
            ),
            noop
        );
    }

    removeStory() {
        this.removeStorySubject.next();
    }

    public drop(event: CdkDragDrop<KanbanCard[], any>): void {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            transferArrayItem(
                event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex
            );
        }
        this.dataStorageService.saveSubject.next();
    }

    save(name: string) {
        this.subscribe(
            this.storyService.saveOrCreateWithName(this.board, this.story, name),
            noop
        );
    }
}
