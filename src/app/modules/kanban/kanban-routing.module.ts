import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {KanbanBoardComponent} from './components/kanban-board/kanban-board.component';
import {KanbanListsComponent} from "@app/modules/kanban/components/kanban-lists/kanban-lists.component";

const routes: Routes = [
    {
        path: '',
        component: KanbanListsComponent,
    },
    {
        path: ':id',
        component: KanbanBoardComponent
    }
];

@NgModule({
              imports: [RouterModule.forChild(routes)],
              exports: [RouterModule]
          })
export class KanbanRoutingModule {
}
