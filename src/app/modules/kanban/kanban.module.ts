import {DragDropModule} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {KanbanBoardComponent} from './components/kanban-board/kanban-board.component';
import {KanbanCardComponent} from './components/kanban-card/kanban-card.component';
import {KanbanStoryComponent} from './components/kanban-story/kanban-story.component';
import {KanbanCardViewerDialogComponent} from './components/kanban-card-viewer/kanban-card-viewer-dialog.component';

import {KanbanRoutingModule} from './kanban-routing.module';
import {SharedModule} from "@app/shared/shared.module";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import { KanbanListsComponent } from './components/kanban-lists/kanban-lists.component';
import {MatListModule} from "@angular/material/list";

@NgModule({
    imports: [
        CommonModule,
        KanbanRoutingModule,
        MatButtonModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        DragDropModule,
        SharedModule,
        MatIconModule,
        MatMenuModule,
        MatDialogModule,
        MatListModule,
        FormsModule
    ],
    declarations: [
        KanbanBoardComponent,
        KanbanStoryComponent,
        KanbanCardComponent,
        KanbanCardViewerDialogComponent,
        KanbanListsComponent,
    ]
})
export class KanbanModule {
}
