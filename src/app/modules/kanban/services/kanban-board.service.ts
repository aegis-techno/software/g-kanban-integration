import {Objects} from '@aegis-techno/ngx-common';
import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {KanbanBoard} from '@app/shared/models/kanban-board';
import {filter, map, tap} from 'rxjs/operators';
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {ConfirmationDialogComponent} from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import {Utils} from "@app/shared/tools/utils";
import {Data} from "@app/shared/models/data";
import {of} from "rxjs";

@Injectable({
    providedIn: "root"
})
export class KanbanBoardService {

    constructor(
        private matDialog: MatDialog,
        private dataStorageService: DataStorageService) {
    }

    createBoard(parent: Data, name: string) {
        return of({name, parentId: parent.id}).pipe(
            map(obj => KanbanBoardService.defaultBoard().updateFromObject(obj)),
            tap((card: KanbanBoard) => parent.boards.push(card)),
            tap((_) => this.dataStorageService.saveSubject.next())
        );
    }

    editBoard(board: KanbanBoard, name: string) {
        return of({name}).pipe(
            map(obj => board.updateFromObject(obj)),
            tap((_) => this.dataStorageService.saveSubject.next())
        );
    }

    removeBoard(board: KanbanBoard) {
        return this.matDialog.open(ConfirmationDialogComponent, {
            data: {
                title: "Suppression d'un board ?"
            }
        }).afterClosed().pipe(
            filter(Objects.isNotNullOrUndefined),
            filter(res => !!res),
            tap(() => {
                let parent = Data.findById<Data>(board.parentId);
                parent!.boards.splice(parent!.boards.indexOf(board), 1)
            }),
            tap(() => this.dataStorageService.saveSubject.next())
        );
    }


    public static defaultBoard(): KanbanBoard {
        const boardId = Utils.newId();
        let board = KanbanBoard.fromObject<KanbanBoard>({id: boardId});
        board.updateFromObject(
            {
                id: boardId,
                name: "Default Board",
                stories: [
                    {
                        id: Utils.newId(),
                        name: 'TODO',
                        parent: board
                    }, {
                        id: Utils.newId(),
                        name: 'DOING',
                        parent: board
                    }, {
                        id: Utils.newId(),
                        name: 'DONE',
                        parent: board
                    }
                ]
            });
        return board;
    }
}
