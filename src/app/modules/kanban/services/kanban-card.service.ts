import {Objects} from '@aegis-techno/ngx-common';
import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {KanbanCardViewerDialogComponent} from '@app/modules/kanban/components/kanban-card-viewer/kanban-card-viewer-dialog.component';
import {KanbanCard} from '@app/shared/models/kanban-card';
import {KanbanStory} from '@app/shared/models/kanban-story';
import {filter, map, tap} from 'rxjs/operators';
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {ConfirmationDialogComponent} from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import {Utils} from "@app/shared/tools/utils";
import {Observable, of} from "rxjs";
import {CustomField} from "@app/shared/models/custom-field";
import * as moment from "moment";

@Injectable({
    providedIn: "root"
})
export class KanbanCardService {

    constructor(
        private matDialog: MatDialog,
        private dataStorageService: DataStorageService
    ) {
    }

    saveOrCreate(story: KanbanStory | undefined, card: KanbanCard | undefined, name: string) {
        if (Objects.isNullOrUndefined(card))
            return this.createCard(story!, name)
        return this.updateCard(card, name);
    }

    createCard(parent: KanbanStory, cardName: string) {
        return of({
            name: cardName,
            id: Utils.newId(),
            parentId: parent.id,
            createDate: moment().format("YYYY-DD-MM")
        } as any).pipe(
            map(obj => KanbanCard.fromObject<KanbanCard>(obj)),
            tap((card: KanbanCard) => parent.cards.push(card)),
            tap((card: KanbanCard) => this.initializeCustomFieldValues(card)),
            tap(() => this.dataStorageService.saveSubject.next())
        )
    }

    updateCard(card: KanbanCard, cardName: string): Observable<any> {
        return of({name: cardName}).pipe(
            map(obj => card.updateFromObject(obj)),
            tap((card: KanbanCard) => this.archiveCustomFieldValues(card)),
            tap((_) => this.dataStorageService.saveSubject.next())
        );
    }

    updateCardField(card: KanbanCard, customField: CustomField, value: any) {
        return of(card.customFieldValues).pipe(
            tap(obj => obj[customField.id!] = value),
            map(obj => card.updateFromObject({customFieldValues: obj})),
            tap((card: KanbanCard) => this.archiveCustomFieldValues(card)),
            tap((_) => this.dataStorageService.saveSubject.next())
        );
    }

    viewCard(card: KanbanCard) {
        return this.matDialog.open(KanbanCardViewerDialogComponent, {
            data: {card}
        }).afterClosed().pipe(
            filter(Objects.isNotNullOrUndefined),
            map(obj => card.updateFromObject(obj)),
            tap((_) => this.dataStorageService.saveSubject.next())
        );
    }

    removeCard(card: KanbanCard) {
        return this.matDialog.open(ConfirmationDialogComponent, {
            data: {
                title: "Suppression d'une card ?"
            }
        }).afterClosed().pipe(
            filter(Objects.isNotNullOrUndefined),
            filter(res => !!res),
            tap(() => {
                let parent = KanbanStory.findById<KanbanStory>(card.parentId);
                parent!.cards.splice(parent!.cards.indexOf(card), 1)
            }),
            tap(() => this.dataStorageService.saveSubject.next())
        );
    }

    private initializeCustomFieldValues(card: KanbanCard) {
        this.dataStorageService.$data.getValue()?.customFields.forEach(
            customField => card.customFieldValues[customField.id!] = customField.defaultValue
        )

        this.archiveCustomFieldValues(card);
    }

    private archiveCustomFieldValues(card: KanbanCard) {
        card.archiveCustomFieldValues[moment().format("YYYY-DD-MM")] = card.customFieldValues;
    }
}
