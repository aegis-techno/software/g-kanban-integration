import {Objects} from '@aegis-techno/ngx-common';
import { Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {KanbanBoard} from '@app/shared/models/kanban-board';
import {KanbanStory} from '@app/shared/models/kanban-story';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import {DataStorageService} from "@app/shared/services/data-storage.service";
import {ConfirmationDialogComponent} from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import {Utils} from "@app/shared/tools/utils";
import {iif, Observable, of} from "rxjs";

@Injectable({
    providedIn: "root"
})
export class KanbanStoryService {

    constructor(
        private matDialog: MatDialog,
        private dataStorageService: DataStorageService) {
    }

    saveOrCreateWithName(boardOrUndefined: KanbanBoard | undefined, storyOrUndefined: KanbanStory | undefined, name: string) {
        return iif(
            () => Objects.isNullOrUndefined(storyOrUndefined),
            this.createStory(boardOrUndefined!),
            of(storyOrUndefined as KanbanStory)
        ).pipe(
            switchMap((story) => this.editStoryWithName(story, name))
        )
    }

    createStory(parent: KanbanBoard): Observable<KanbanStory> {
        return of({id : Utils.newId(), parentId: parent.id}).pipe(
            map(obj => KanbanStory.fromObject<KanbanStory>(obj)),
            tap((card: KanbanStory) => parent.stories.push(card)),
        );
    }

    editStoryWithName(story: KanbanStory, name: string) {
        return of({name: name}).pipe(
            map(obj => story.updateFromObject(obj)),
            tap((_) => this.dataStorageService.saveSubject.next())
        );
    }

    askForRemoveStory(story: KanbanStory) {
        return this.matDialog.open(ConfirmationDialogComponent, {
            data: {
                title: "Suppression d'une story ?"
            }
        }).afterClosed().pipe(
            filter(Objects.isNotNullOrUndefined),
            filter(res => !!res),
            switchMap(() => this.removeStory(story))
        );
    }

    removeStory(story: KanbanStory) {
        return of(null).pipe(
            tap(() => {
                let parent = KanbanBoard.findById<KanbanBoard>(story.parentId);
                parent!.stories.splice(parent!.stories.indexOf(story), 1)
            }),
            tap(() => this.dataStorageService.saveSubject.next())
        );
    }
}
