import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsAdministrationComponent } from './statistics-administration.component';

describe('StatisticsAdministrationComponent', () => {
  let component: StatisticsAdministrationComponent;
  let fixture: ComponentFixture<StatisticsAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatisticsAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
