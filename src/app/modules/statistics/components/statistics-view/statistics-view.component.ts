import {Component, OnInit} from '@angular/core';
import {filter, map, switchMap, toArray} from "rxjs/operators";
import {Objects} from "@aegis-techno/ngx-common";
import {Data} from "@app/shared/models/data";
import {from, Observable, of, Subject, zip} from "rxjs";
import {KanbanBoard} from "@app/shared/models/kanban-board";
import {DataStorageService} from "@app/shared/services/data-storage.service";

@Component({
    selector: 'app-statistics-view',
    templateUrl: './statistics-view.component.html',
    styleUrls: ['./statistics-view.component.scss']
})
export class StatisticsViewComponent implements OnInit {

    $boardOptions: Observable<any> | undefined;
    $displayStats: Observable<any> | undefined;
    $currentBoard: Observable<KanbanBoard> | undefined;
    private refreshStatsSubject: Subject<any> = new Subject<any>();


    constructor(private dataStorageService: DataStorageService) {
    }

    ngOnInit(): void {
        this.$boardOptions = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            switchMap((data: Data) => from(data.boards).pipe(
                map((board: KanbanBoard) => {
                    return {label: board.name, value: board.id}
                }),
                toArray()
            ))
        );

        this.$currentBoard = this.dataStorageService.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            map(value => value.boards[0])
        );

        this.$displayStats = this.refreshStatsSubject.pipe(
            switchMap(() => this.$currentBoard!),
            map((board: KanbanBoard) => {
                return this.dataStorageService.$data.getValue()!.stats.filter(value => value.boardIds.includes(board.id!))
            })
        );

        this.refreshStatsSubject.next();
    }

    selectCurrentBoard(boardId: string) {
        this.$currentBoard = of(KanbanBoard.findById<KanbanBoard>(boardId)!);
        this.refreshStatsSubject.next();
    }

}
