import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatisticsRoutingModule } from './statistics-routing.module';
import { StatisticsHomeComponent } from './components/statistics-home/statistics-home.component';
import {MatIconModule} from "@angular/material/icon";
import {MatTabsModule} from "@angular/material/tabs";
import {SharedModule} from "@app/shared/shared.module";
import { StatisticsViewComponent } from './components/statistics-view/statistics-view.component';
import { StatisticsAdministrationComponent } from './components/statistics-administration/statistics-administration.component';


@NgModule({
  declarations: [
    StatisticsHomeComponent,
    StatisticsViewComponent,
    StatisticsAdministrationComponent
  ],
    imports: [
        CommonModule,
        StatisticsRoutingModule,
        MatIconModule,
        MatTabsModule,
        SharedModule
    ]
})
export class StatisticsModule { }
