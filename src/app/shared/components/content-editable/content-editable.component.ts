import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BaseComponent, Objects} from "@aegis-techno/ngx-common";
import {InputType} from "@app/shared/models/custom-field";

@Component({
    selector: 'app-content-editable',
    templateUrl: './content-editable.component.html',
    styleUrls: ['./content-editable.component.scss']
})
export class ContentEditableComponent extends BaseComponent implements OnInit {

    @Output()
    public save: EventEmitter<any> = new EventEmitter<any>()

    @Input()
    public value: any;

    @Input()
    public type: InputType = 'text';

    @Input()
    public selectOptions: any[] = [];

    public isEditing: boolean = false;
    public valueEditing: any;

    constructor(private elementRef: ElementRef) {
        super();
    }

    ngOnInit(): void {
    }

    edit(event: MouseEvent) {
        this.stopPropagation(event);

        this.valueEditing = this.value;
        this.isEditing = true;
        this.defer(() => {
            let suffix = '';
            if (this.type === "select") {
                suffix = ' .mat-select-trigger'
            }
            this.elementRef.nativeElement.querySelector('.autofocusable' + suffix)?.focus()
            this.elementRef.nativeElement.querySelector('.autoclickable' + suffix)?.click()
        })
    }

    reset() {
        this.valueEditing = '';
        this.isEditing = false;
    }

    validate(value?: any) {
        if(Objects.isNotNullOrUndefined(value)){
            this.valueEditing = value;
        }
        if (this.type === 'number' && typeof this.valueEditing !== 'number') {
            try {
                this.valueEditing = parseInt(this.valueEditing).toString(10);
            } catch(e) {
            }
        }
        this.save.next(this.valueEditing);
        this.reset();
    }
}
