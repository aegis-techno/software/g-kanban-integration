import {NgxEntity, NgxRegisteredEntity} from "@aegis-techno/ngx-entity-manager";

export type InputType = 'text' | 'number' | 'boolean' | 'date' | 'textarea' | 'select'

@NgxEntity()
export class CustomField extends NgxRegisteredEntity<string> {
    label:string = 'Default label';

    type: InputType = 'text';
    defaultValue: any = '';
    visibility: boolean = false;
}
