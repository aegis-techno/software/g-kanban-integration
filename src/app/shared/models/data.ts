import {NgxEntity, NgxRegisteredEntity, NgxRelation} from "@aegis-techno/ngx-entity-manager";
import {forwardRef} from "@angular/core";
import {KanbanBoard} from "@app/shared/models/kanban-board";
import {CustomField} from "@app/shared/models/custom-field";
import {Statistics} from "@app/shared/models/statistics";

@NgxEntity()
export class Data extends NgxRegisteredEntity<string> {

    @NgxRelation(forwardRef(() => KanbanBoard), true)
    boards: KanbanBoard[] = [];

    @NgxRelation(forwardRef(() => CustomField), true)
    customFields: CustomField[] = [];

    @NgxRelation(forwardRef(() => Statistics), true)
    stats: Statistics[] = [];

}
