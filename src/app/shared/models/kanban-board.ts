import {NgxEntity, NgxRegisteredEntity, NgxRelation} from '@aegis-techno/ngx-entity-manager';
import {forwardRef} from '@angular/core';
import {KanbanStory} from './kanban-story';

@NgxEntity()
export class KanbanBoard extends NgxRegisteredEntity<string> {

    name: string = '';

    @NgxRelation(forwardRef(() => KanbanStory), true)
    stories: KanbanStory[] = [];

    parentId: string = '';
}
