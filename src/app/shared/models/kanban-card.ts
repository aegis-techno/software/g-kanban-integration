import {NgxEntity, NgxRegisteredEntity} from '@aegis-techno/ngx-entity-manager';

@NgxEntity()
export class KanbanCard extends NgxRegisteredEntity<string> {

    name: string = '';
    createDate: string = '';

    parentId: string = '';

    customFieldValues: any = {};
    archiveCustomFieldValues: any = {};
}
