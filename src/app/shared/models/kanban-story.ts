import {NgxEntity, NgxExcludeFromJSON, NgxRegisteredEntity, NgxRelation} from '@aegis-techno/ngx-entity-manager';
import {forwardRef} from '@angular/core';
import {KanbanCard} from '@app/shared/models/kanban-card';
import {Optional} from "@aegis-techno/ngx-common";
import {KanbanBoard} from "@app/shared/models/kanban-board";

@NgxEntity()
export class KanbanStory extends NgxRegisteredEntity<string> {
    name: string = '';

    @NgxRelation(forwardRef(() => KanbanCard), true)
    cards: KanbanCard[] = [];

    parentId: string = '';
}
