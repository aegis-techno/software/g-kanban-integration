import {NgxEntity, NgxRegisteredEntity} from "@aegis-techno/ngx-entity-manager";

@NgxEntity()
export class Statistics extends NgxRegisteredEntity<string> {
    label:string = 'Default label';

    boardIds: string[] = []

}
