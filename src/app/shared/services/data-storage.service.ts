import {Injectable} from '@angular/core';
import {catchError, filter, map, mapTo, switchMap, takeUntil, tap, throttleTime} from 'rxjs/operators';
import {BehaviorSubject, defer, iif, Observable, of, Subject} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {GDriveState} from "@app/shared/services/g-drive.state";
import {Objects, Optional} from "@aegis-techno/ngx-common";
import {KanbanBoard} from "@app/shared/models/kanban-board";
import {GoogleDriveStorage} from "@app/shared/services/google-drive-storage.service";
import {MatDialog} from "@angular/material/dialog";
import {Utils} from "@app/shared/tools/utils";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LocalStorage} from "@app/shared/services/local-storage.service";
import {Storage} from "@app/shared/services/storage";
import {Data} from "@app/shared/models/data";
import {KanbanBoardService} from "@app/modules/kanban/services/kanban-board.service";

@Injectable({
    providedIn: 'root'
})
export class DataStorageService {

    public saveSubject: Subject<void> = new Subject<void>();
    public loadSubject: Subject<void> = new Subject<void>();

    public $data: BehaviorSubject<Optional<Data>> = new BehaviorSubject<Optional<Data>>(undefined);

    private activeStorage: Storage;
    private googleDriveState: Observable<GDriveState>;

    constructor(
        private activatedRouteSnapshot: ActivatedRoute,
        private googleDriveStorage: GoogleDriveStorage,
        private localStorage: LocalStorage,
        private matDialog: MatDialog,
        private snackBar: MatSnackBar,
    ) {
        this.activeStorage = localStorage;
        this.googleDriveState = this.googleDriveStorage.$state.pipe(
            filter(Objects.isNotNullOrUndefined)
        );
        this.loadData().pipe(
            takeUntil(this.googleDriveState),
            catchError(() => this.createNewData()),
            filter(Objects.isNotNullOrUndefined),
            tap(() => this.loadSubject.next()),
            tap(() => this.snackBar.open('Chargement des données local !', '', DataStorageService.getSnackConfig()))
        ).subscribe();

        this.googleDriveState.pipe(
            tap(() => this.activeStorage = googleDriveStorage),
            switchMap((state) => this.loadOrCreate(state))
        ).subscribe()

        this.saveSubject.pipe(
            throttleTime(300),
            switchMap(() => this.save())
        ).subscribe();
    }

    private loadOrCreate(state: GDriveState): Observable<Data> {

        return of(null).pipe(
            switchMap(() => iif(
                () => state.action === 'create',
                defer(() => this.createNewData()),
                defer(() => this.loadData()),
            )),
            switchMap(() => this.$data),
            filter(Objects.isNotNullOrUndefined),
            tap(() => this.loadSubject.next()),
            tap(() => this.snackBar.open('Chargement des données terminé !', '', DataStorageService.getSnackConfig()))
        );
    }

    private save(): Observable<void> {
        return this.$data.pipe(
            filter(Objects.isNotNullOrUndefined),
            map((data) => DataStorageService.stringify(data)),
            switchMap((data) => this.activeStorage.saveFile(data)),
            tap(() => this.snackBar.open('Sauvegarde des données terminé !', '', DataStorageService.getSnackConfig()))
        );
    }

    private static getSnackConfig(): any {
        return {
            horizontalPosition: "end",
            verticalPosition: "top",
            duration: 2000
        };
    }

    private loadData(): Observable<Data> {
        return this.activeStorage.loadFile().pipe(
            map((data) => DataStorageService.parseData(data)),
            tap((data) => this.$data.next(data))
        );
    }

    private static parseData(data: string): Data {
        return Data.fromObject(JSON.parse(data));
    }

    private createNewData(): Observable<Data> {
        let defaultData = DataStorageService.defaultData();
        return this.activeStorage.createFile(DataStorageService.stringify(defaultData)).pipe(
            tap(() => this.$data.next(defaultData)),
            mapTo(defaultData)
        )
    }

    private static stringify(data: Data): string {
        return JSON.stringify(data.toJSON(true));
    }

    private static defaultData(): Data {
        const board = KanbanBoardService.defaultBoard();
        return Data.fromObject({
            id: Utils.newId(),
            boards:[board]
        });
    }
}
