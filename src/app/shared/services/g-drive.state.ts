export interface GDriveState {
    ids: string,
    folderId: string,
    action: 'create' | 'open',
    userId: string,
    folderResourceKey: string,
    resourceKey: string
}


export interface GDriveStateCreate  extends GDriveState {
    folderId: string,
    action: 'create',
    folderResourceKey: string,
}

export interface GDriveStateOpen extends GDriveState{
    ids: string,
    action: 'open',
    resourceKey: string
}
