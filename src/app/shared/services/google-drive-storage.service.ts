import {Injectable, NgZone} from "@angular/core";
import {BehaviorSubject, from, iif, Observable, of} from "rxjs";
import {GDriveState} from "@app/shared/services/g-drive.state";
import {ActivatedRoute} from "@angular/router";
import {delay, filter, first, map, mergeMap, pluck, switchMap, tap} from "rxjs/operators";
import {Objects, Strings} from "@aegis-techno/ngx-common";
import {Storage} from "./storage";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class GoogleDriveStorage implements Storage {

    public $state: BehaviorSubject<GDriveState | undefined> = new BehaviorSubject<GDriveState | undefined>(undefined);

    public mimeType = 'application/json+g.kanban.integration';
    private isInit: boolean = false;
    private isSignedIn: BehaviorSubject<boolean>  = new BehaviorSubject<boolean>(false);


    constructor(
        private activatedRouteSnapshot: ActivatedRoute,
        private ngZone: NgZone
    ) {
        this.activatedRouteSnapshot.queryParams.pipe(
            delay(200),
            first(),
            pluck('state')
        ).subscribe((state: string) => {
            if (Strings.isBlank(state)) {
                this.$state.next(undefined);
            } else {
                this.$state.next(JSON.parse(state));
            }
        })
    }


    signIn(): Observable<boolean> {
        return iif(
            () => !this.isInit,
            this.initClient(),
            of(null)
        ).pipe(
            switchMap(
                () => iif(
                    () => !this.isSignedIn.value,
                    of(false).pipe(
                        tap(() => gapi.auth2.getAuthInstance().signIn()),
                        switchMap(() => this.isSignedIn.asObservable().pipe(
                            filter(res => res))
                        )
                    ),
                    of(true)
                )
            )
        );
    }

    public initClient(): Observable<any> {
        return new Observable<any>(subscriber => {
            gapi.load('client:auth2', () => {
                this.ngZone.run(() => {
                    from(gapi.client.init(
                            {
                                apiKey: environment.api_key,
                                clientId: environment.client_id,
                                discoveryDocs: environment.discovery_docs,
                                scope: environment.scopes
                            }
                        )
                    ).pipe(
                        tap(() => {
                            gapi.auth2.getAuthInstance().isSignedIn.listen((isSignedIn: boolean) => {
                                this.ngZone.run(() => {
                                    this.isSignedIn.next(isSignedIn);
                                });
                            });

                            this.ngZone.run(() => {
                                this.isInit = true;
                                this.isSignedIn.next(gapi.auth2.getAuthInstance().isSignedIn.get());
                            });
                        })
                    ).subscribe(subscriber);
                });
            });
        });
    }

    createFile(content: string): Observable<any> {

        var parentId = this.$state.getValue()!.folderId;
        var fileMetadata = {
            'name': 'New Kanban board',
            'mimeType': this.mimeType,
            'parents': [parentId]
        };

        return this.signIn().pipe(
            switchMap(() => this._createFile(fileMetadata)),
            tap((response) => {
                this.$state.getValue()!.action = 'open';
                this.$state.getValue()!.ids = response.result.id;
            }),
            mergeMap(() => this.saveFile(content)),
        );
    }

    _createFile(fileMetadata: any): Observable<any> {
        return new Observable<void>((subscriber) => {
            gapi.client.drive.files.create({
                resource: fileMetadata,
            }).then((response: any) => {
                switch (response.status) {
                    case 200:
                        this.ngZone.run(() => {
                            subscriber.next(response);
                            subscriber.complete();
                        });
                        break;
                    default:
                        this.ngZone.run(() => {
                            subscriber.error(new Error('Error creating the folder'));
                        })
                        break;
                }
            });
        });
    }

    loadFile(): Observable<string> {
        var fileMetadata = {
            'fileId': this.$state.getValue()!.ids,
            'alt': 'media'
        };

        return this.signIn().pipe(
            switchMap(() => this._loadFile(fileMetadata)),
            map((response) => response.body)
        );
    }

    saveFile(content: string): Observable<any> {

        let state = this.$state.getValue();
        if (Objects.isNullOrUndefined(state) || Objects.isNullOrUndefined(state.ids)) {
            return of(null);
        }

        const fileMetadata = {
            'fileId': state.ids,
            'content': content,
            'mimeType': this.mimeType,
        };

        return this.signIn().pipe(
            switchMap(() => this._saveFile(fileMetadata))
        );
    }

    private _loadFile(fileMetadata: any): Observable<any> {
        return new Observable<any>((subscriber) => {
            gapi.client.drive.files.get(fileMetadata).then((response: any) => {
                switch (response.status) {
                    case 200:
                        this.ngZone.run(() => {
                            subscriber.next(response);
                            subscriber.complete();
                        });
                        break;
                    default:
                        this.ngZone.run(() => {
                            subscriber.error(new Error('Error loading the folder'));
                        });
                        break;
                }
            });
        });
    }

    private _saveFile(fileMetadata: any) {
        return new Observable<void>((subscriber) => {
            gapi.client.request({
                path: '/upload/drive/v3/files/' + fileMetadata.fileId,
                method: 'PATCH',
                params: {
                    uploadType: 'media'
                },
                headers: {
                    'Content-type': fileMetadata.mimeType
                },
                body: fileMetadata.content
            }).then((response: any) => {
                switch (response.status) {
                    case 200:
                        this.ngZone.run(() => {
                            subscriber.next(response);
                            subscriber.complete();
                        });
                        break;
                    default:
                        this.ngZone.run(() => {
                            subscriber.error(new Error('Error saving the folder'));
                        });
                        break;
                }
            });
        });
    }
}
