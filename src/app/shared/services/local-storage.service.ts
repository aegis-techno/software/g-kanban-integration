import {Injectable} from "@angular/core";
import {Observable, of, throwError} from "rxjs";
import {Storage} from "./storage";
import {map, tap} from "rxjs/operators";
import {NgxLocalStorageService} from "@aegis-techno/ngx-local-storage";

@Injectable({
    providedIn: 'root'
})
export class LocalStorage implements Storage {

    constructor(private ngxLocalStorageService: NgxLocalStorageService) {
    }

    saveFile(board: string): Observable<any> {
        return of(null).pipe(
            tap(() => this.ngxLocalStorageService.set("data", board))
        )
    }

    loadFile(): Observable<string> {
        return of(null).pipe(
            map(() => this.ngxLocalStorageService.get("data")),
        )
    }

    createFile(board: string): Observable<any> {
        return of(null)
    }

}
