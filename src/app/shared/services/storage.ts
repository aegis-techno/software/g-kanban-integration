import {Observable} from "rxjs";

export interface Storage {
    saveFile(board: string): Observable<any>;

    loadFile(): Observable<string>;

    createFile(board: string): Observable<any>;
}
