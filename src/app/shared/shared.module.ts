import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatDialogModule} from "@angular/material/dialog";
import {LoadingDialogComponent} from "@app/shared/components/loading-dialog/loading-dialog.component";
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import {MatButtonModule} from "@angular/material/button";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { ContentEditableComponent } from './components/content-editable/content-editable.component';
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import {MatCheckboxModule} from "@angular/material/checkbox";

@NgModule({
    imports: [
        CommonModule,
        MatDialogModule,
        MatButtonModule,
        MatDialogModule,
        MatSnackBarModule,
        MatInputModule,
        FormsModule,
        MatSelectModule,
        MatCheckboxModule
    ],
    declarations: [
        LoadingDialogComponent,
        ConfirmationDialogComponent,
        ContentEditableComponent
    ],
    exports: [
        ContentEditableComponent
    ]
})
export class SharedModule {

}
