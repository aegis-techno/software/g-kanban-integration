export class Utils {

    static newId(): string {
        return crypto.getRandomValues(new Uint32Array([0]))[0].toString(10)
    }
}
