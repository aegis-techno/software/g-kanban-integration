export const environment = {
    production: true,
    client_id: '757113694649-f7tvvvq87qe0f0hni4n4gj144e8em5bs.apps.googleusercontent.com',
    api_key: 'AIzaSyDL4Oz8FZYynxT9-DUx7SGi1WVzLV9fUeI',

    discovery_docs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],

    scopes: [
        'https://www.googleapis.com/auth/drive.metadata.readonly',
        'https://www.googleapis.com/auth/drive.file',
        'https://www.googleapis.com/auth/drive.install'
    ].join(' '),

    apiBaseUrl: '',
};
