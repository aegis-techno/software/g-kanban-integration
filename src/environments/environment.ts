// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    client_id: '757113694649-f7tvvvq87qe0f0hni4n4gj144e8em5bs.apps.googleusercontent.com',
    api_key: 'AIzaSyDL4Oz8FZYynxT9-DUx7SGi1WVzLV9fUeI',

    discovery_docs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],

    scopes: [
        'https://www.googleapis.com/auth/drive.metadata.readonly',
        'https://www.googleapis.com/auth/drive.file',
        'https://www.googleapis.com/auth/drive.install'
    ].join(' '),

    apiBaseUrl: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
