# CONDITIONS GÉNÉRALES

Les présentes conditions générales régissent l’utilisation de cet application :
- aegis-techno.gitlab.io/software/g-kanban-integration

Cet url présente une application créé et géré par Boris Bodin

En utilisant cet application, vous indiquez que vous avez lu et compris les conditions d’utilisation et que vous acceptez de les respecter en tout temps.

Type de site : Une application de gestion de tâches

## Propriété intellectuelle

Tout contenu publié et mis à disposition sur cet application est la propriété de Boris Bodin et de ses créateurs. Cela comprend, mais n’est pas limité aux images, textes, logos, documents, fichiers téléchargeables et tout ce qui contribue à la composition de cet application.

## Limitation de responsabilité

Boris Bodin ou l’un de ses employés sera tenu responsable de tout problème découlant de cet application. Néanmoins, Boris Bodin et ses employés ne seront pas tenus responsables de tout problème découlant de toute utilisation irrégulière de cet application.

## Indemnité

En tant qu’utilisateur, vous indemnisez par les présentes Boris Bodin de toute responsabilité, de tout coût, de toute cause d’action, de tout dommage ou de toute dépense découlant de votre utilisation de cet application ou de votre violation de l’une des dispositions énoncées dans le présent document.

## Lois applicables

Ce document est soumis aux lois applicables en France et vise à se conformer à ses règles et règlements nécessaires. Cela inclut la réglementation à l’échelle de l’UE énoncée dans le RGPD.

## Divisibilité

Si, à tout moment, l’une des dispositions énoncées dans le présent document est jugée incompatible ou invalide en vertu des lois applicables, ces dispositions seront considérées comme nulles et seront retirées du présent document. Toutes les autres dispositions ne seront pas touchées par les lois et le reste du document sera toujours considéré comme valide.

## Modifications

Ces conditions générales peuvent être modifiées de temps à autre afin de maintenir le respect de la loi et de refléter tout changement à la façon dont nous gérons notre application et la façon dont nous nous attendons à ce que les utilisateurs se comportent sur notre application. Nous recommandons à nos utilisateurs de vérifier ces conditions générales de temps à autre pour s’assurer qu’ils sont informés de toute mise à jour. Au besoin, nous informerons les utilisateurs par courriel des changements apportés à ces conditions ou nous afficherons un avis sur notre application.

## Contact

Veuillez communiquer avec nous si vous avez des questions ou des préoccupations. Nos coordonnées sont les suivantes :

boris.bodin@aegis-techno.fr
